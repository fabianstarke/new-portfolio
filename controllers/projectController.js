const asyncHandler = require('express-async-handler');
const Project = require('../models/projectModel');
const User = require('../models/userModel');


//@desc Get Projects
// @route GET /api/projects
// @access Private
const getProjects = asyncHandler(async (req, res) => {

    let project = [];
    if (req.params.id) {
        project = await Project.findById(req.params.id);
    } else {
        project = await Project.find({ user: req.user.id});
    }

    res.status(200).json(project);

})

//@desc Set Project
// @route POST /api/projects
// @access Private
const setProject = asyncHandler(async (req, res) => {

    if(!req.body){
        return res.status(400).json({
            message: 'Error with request body'
        })
    }

    Object.entries(req.body).forEach(([key, value]) => {
        if (value === '' || value === null) {
            return res.status(400).json({
                success: false,
                message: `${key} is required`
            })
        }
    }
    )

    const project = await Project.create({
       ...req.body,
         user: req.user.id
    });
    res.status(200).json({ success: true, message: `Project ${req.body.name} created successfully`});
})

//@desc Update Project
// @route PUT /api/projects/:id
// @access Private
const updateProject = asyncHandler(async (req, res) => {

    const project = await Project.findById(req.params.id);
    if(!project){
        res.status(400);
        throw new Error('Project does not exist');
    }

    // Check for user
    if(!req.user){
        res.status(401);
        throw new Error('User does not exist');
    }

    // Make sure the user is the one who created the project
    if(project.user.toString() !== req.user.id){
        res.status(401);
        throw new Error('User not authorized');
    }

    const updateProject = await Project.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
    })

    res.status(200).json(updateProject);
})

//@desc Delete Project
// @route DELETE /api/projects/:id
// @access Private
const deleteProject = asyncHandler(async (req, res) => {
    const project = await Project.findById(req.params.id);
    if(!project){
        res.status(400);
        throw new Error('Project does not exist');
    }

    // Check for user
    if(!req.user){
        res.status(401);
        throw new Error('User does not exist');
    }

    // Make sure the user is the one who created the project
    if(project.user.toString() !== req.user.id){
        res.status(401);
        throw new Error('User not authorized');
    }

    await project.remove();

    res.status(200).json({ id: req.params.id });
})



module.exports = {
    getProjects,
    setProject,
    updateProject,
    deleteProject
}