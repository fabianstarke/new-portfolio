const mongoose = require('mongoose');

const projectSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    name: {
        type: String,
        required: [true, 'Please add a name']
    },
    type: {
        type: String,
        required: [true, 'Please add a type']
    },
    description: {
        type: String,
        required: [true, 'Please add a description']
    },
    text: {
        type: String,
        required: [true, 'Please add a text']
    },
    imageUrl: {
        type: String,
        required: [true, 'Please add an image']
    },
    repoLink: {
        type: String,
        required: false
    },
    websiteLink: {
        type: String,
        required: false
    },
    skills: {
        type: [String],
        required: [true, 'Please add at least one skill']
    },
    
}, { timestamps: true });


module.exports = mongoose.model('Project', projectSchema);